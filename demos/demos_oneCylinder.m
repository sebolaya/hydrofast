clear; close all; clc;

water_depth = 150;

geom.Rbo = 5;
geom.b = 5;

options.Truncate.Ni = 50;
options.Truncate.Nj = 50;

options.Zc = 0;

dw=.1;
Omega = (.1:dw:3)';

[Fe, A, B, A_inf, Fe_Haskind, Fe_FK] = oneCylinder(Omega, water_depth, geom, options);

i=2; j=2;
Ma(:,1)=A(i,j,:);
Ma_inf(:,1)=A_inf(i,j,:)*ones(length(Omega),1);
Fex(:,1)=Fe(:,i);


figure, grid on, hold on;
[hAx]=plot(Omega,Ma,Omega,Ma_inf, '-.b');
xlabel('$$\omega$$ [rad/s]', 'Interpreter', 'latex', 'FontSize', 12);
ylabel('added mass [kg]')

figure, grid on, hold on;
[hAx, hL1, hL2] = plotyy(Omega, abs(Fex), Omega, angle(Fex));
xlabel('$$\omega$$ [rad/s]', 'Interpreter', 'latex', 'FontSize', 12);
title('Excitation force $$F_e$$', 'Interpreter', 'latex', 'FontSize', 12)
